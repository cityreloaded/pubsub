package de.paxii.pubsub.rabbitmq.command;

import de.paxii.pubsub.command.CommandHandler;
import de.paxii.pubsub.command.CommandInterface;

/**
 * Created by Lars on 02.02.2017.
 */
public class RabbitMqCommandHandler implements CommandHandler {

  @Override
  public void handleCommand(CommandInterface command) {
    command.execute();
  }

}
