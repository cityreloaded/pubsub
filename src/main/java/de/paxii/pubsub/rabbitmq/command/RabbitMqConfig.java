package de.paxii.pubsub.rabbitmq.command;

import java.util.ArrayList;

import lombok.Data;

/**
 * Created by Lars on 12.02.2017.
 */
@Data
public class RabbitMqConfig {

  private String queueName;

  private String exchangeName;

  private ArrayList<String> topics;

  private String defaultTopic;

  public RabbitMqConfig(String queueName, String exchangeName, String defaultTopic) {
    this.queueName = queueName;
    this.exchangeName = exchangeName;
    this.topics = new ArrayList<>(5);
  }

  public void addTopic(String topic) {
    this.topics.add(topic);
  }

  public void removeTopic(String topic) {
    this.topics.remove(topic);
  }

  public String[] getTopics() {
    return this.topics.toArray(new String[this.topics.size()]);
  }
}
