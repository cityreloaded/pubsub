package de.paxii.pubsub.rabbitmq.command;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import de.paxii.pubsub.command.CommandBusInterface;
import de.paxii.pubsub.command.CommandInterface;
import de.paxii.pubsub.command.CommandSerializer;

import java.io.IOException;

/**
 * Created by Lars on 02.02.2017.
 */
public class RabbitMqCommandBus implements CommandBusInterface {

  private String queueName;

  private String exchangeName;

  private String defaultTopic;

  private String[] topics;

  private CommandSerializer commandSerializer;

  private RabbitMqConnectionManager rabbitMqConnectionManager;

  private RabbitMqCommandHandler rabbitMqCommandHandler;

  public RabbitMqCommandBus(RabbitMqConfig config, RabbitMqConnectionManager rabbitMqConnectionManager) {
    this(
            config.getQueueName(),
            config.getExchangeName(),
            config.getTopics(),
            config.getDefaultTopic(),
            rabbitMqConnectionManager
    );
  }

  public RabbitMqCommandBus(
          String queueName,
          String exchangeName,
          String[] topics,
          String defaultTopic,
          RabbitMqConnectionManager rabbitMqConnectionManager
  ) {
    this.queueName = queueName;
    this.exchangeName = exchangeName;
    this.topics = topics;
    this.defaultTopic = defaultTopic;
    this.commandSerializer = new CommandSerializer();
    this.rabbitMqConnectionManager = rabbitMqConnectionManager;
    this.rabbitMqCommandHandler = new RabbitMqCommandHandler();
  }

  public void dispatchCommand(CommandInterface command) {
    this.dispatchCommand(command, this.defaultTopic);
  }

  public void dispatchCommand(CommandInterface command, String topic) {
    String serializedCommand = this.commandSerializer.serializeCommand(command);

    try {
      this.rabbitMqConnectionManager.basicExchangePublishWithTopic(this.exchangeName, topic, serializedCommand);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void registerConsumer() {
    try {
      Consumer consumer = new DefaultConsumer(this.rabbitMqConnectionManager.getChannel()) {
        @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
          String jsonString = new String(body, "UTF-8");
          CommandInterface command = RabbitMqCommandBus.this.commandSerializer.deserializeCommand(jsonString);
          RabbitMqCommandBus.this.rabbitMqCommandHandler.handleCommand(command);
        }
      };
      for (String topic : this.topics) {
        this.rabbitMqConnectionManager.bindQueueWithTopic(this.queueName, this.exchangeName, topic);
      }
      this.rabbitMqConnectionManager.basicConsume(this.queueName, consumer);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
