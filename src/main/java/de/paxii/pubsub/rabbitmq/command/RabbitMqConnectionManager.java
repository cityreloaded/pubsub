package de.paxii.pubsub.rabbitmq.command;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;

import java.io.IOException;

import lombok.Getter;

/**
 * Created by Lars on 02.02.2017.
 */
public class RabbitMqConnectionManager {

  private ConnectionFactory connectionFactory;

  private Connection connection;

  @Getter
  private Channel channel;

  public RabbitMqConnectionManager() {
    this.connectionFactory = new ConnectionFactory();
  }

  public void setCredentials(String hostname, String username, String password) {
    this.connectionFactory.setHost(hostname);
    this.connectionFactory.setUsername(username);
    this.connectionFactory.setPassword(password);
  }

  public void initializeConnection() throws Exception {
    this.connection = this.connectionFactory.newConnection();
    this.channel = this.connection.createChannel();
  }

  public AMQP.Queue.DeclareOk declareQueue() throws IOException {
    return this.channel.queueDeclare();
  }

  public AMQP.Queue.DeclareOk declareQueue(String queueName) throws IOException {
    return this.channel.queueDeclare(queueName, false, false, false, null);
  }

  public AMQP.Exchange.DeclareOk declareExchange(String exchangeName) throws IOException {
    return this.channel.exchangeDeclare(exchangeName, "topic");
  }

  public void basicQueuePublish(String queueName, String message) throws IOException {
    this.channel.basicPublish("", queueName, null, message.getBytes());
  }

  public void basicExchangePublish(String queueName, String message) throws IOException {
    this.channel.basicPublish(queueName, "", null, message.getBytes());
  }

  public void basicExchangePublishWithTopic(String queueName, String topic, String message) throws IOException {
    this.channel.basicPublish(queueName, topic, null, message.getBytes());
  }

  public void basicConsume(String queueName, Consumer consumer) throws IOException {
    this.channel.basicConsume(queueName, true, consumer);
  }

  public AMQP.Queue.BindOk bindQueue(String queueName, String exchangeName) throws IOException {
    return this.channel.queueBind(queueName, exchangeName, "");
  }

  public AMQP.Queue.BindOk bindQueueWithTopic(String queueName, String exchangeName, String topic) throws IOException {
    return this.channel.queueBind(queueName, exchangeName, topic);
  }

}
