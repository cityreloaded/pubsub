package de.paxii.pubsub.command;

/**
 * Created by Lars on 02.02.2017.
 */
public interface CommandBusInterface {

  void dispatchCommand(CommandInterface command);

}
