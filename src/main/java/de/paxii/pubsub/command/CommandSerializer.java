package de.paxii.pubsub.command;

import com.google.gson.Gson;

/**
 * Created by Lars on 02.02.2017.
 */
public class CommandSerializer {

  private Gson gson;

  public CommandSerializer() {
    this.gson = new Gson();
  }

  public String serializeCommand(CommandInterface command) {
    SerializedCommand serializedCommand = new SerializedCommand(command.getClass().getName(), this.gson.toJson(command));
    return this.gson.toJson(serializedCommand);
  }

  @SuppressWarnings(value = "unchecked")
  public CommandInterface deserializeCommand(String jsonString) {
    SerializedCommand serializedCommand = this.gson.fromJson(jsonString, SerializedCommand.class);
    try {
      return this.gson.fromJson(
              serializedCommand.getProperties(),
              (Class<CommandInterface>) Class.forName(serializedCommand.getClassName())
      );
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }

  @SuppressWarnings(value = "unchecked")
  public <T> T deserializeCommand(String jsonString, T type) {
    return (T) this.deserializeCommand(jsonString);
  }

  public <T> T deserializeCommand(String jsonString, Class<T> type) {
    return type.cast(this.deserializeCommand(jsonString));
  }

}
