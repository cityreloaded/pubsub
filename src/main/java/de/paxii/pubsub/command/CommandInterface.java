package de.paxii.pubsub.command;

/**
 * Created by Lars on 02.02.2017.
 */
public interface CommandInterface {

  void execute();

}
