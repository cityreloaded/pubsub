package de.paxii.pubsub.command;

import lombok.Getter;

/**
 * Created by Lars on 02.02.2017.
 */
public class SerializedCommand {

  @Getter
  private String className;

  @Getter
  private String properties;

  public SerializedCommand(String className, String properties) {
    this.className = className;
    this.properties = properties;
  }

}
